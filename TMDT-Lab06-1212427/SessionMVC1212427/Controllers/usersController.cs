﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SessionMVC1212427.DAO;
using SessionMVC1212427.Models;

namespace SessionMVC1212427.Controllers
{
    public class usersController : Controller
    {
        //
        // GET: /users/

        public ActionResult Index1212427()
        {
            if (System.Web.HttpContext.Current.Session["account"] == null)
            {
               return RedirectToAction("Index1212427", "auth", new { returnUrl = Request.Url.ToString()});
            }
            ViewBag.Title = "Users";
            //ViewBag.users = new users().Users;
            return View(new users().Users);
        }

        public ActionResult detail1212427(string id)
        {
            if (System.Web.HttpContext.Current.Session["account"] == null)
            {
                return RedirectToAction("Index1212427", "auth", new { returnUrl = Request.Url.ToString() });
            }
            ViewBag.Title = id;
            user u = usersDAO.Find(id);
            return View(u);
        }
    }
}
