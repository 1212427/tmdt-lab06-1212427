﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SessionMVC1212427.DAO;
using SessionMVC1212427.Models;

namespace SessionMVC1212427.Controllers
{
    public class authController : Controller
    {
        //
        // GET: /Account/

        public ActionResult Index1212427()
        {
            ViewBag.Title = "Login";
            return View();
        }
        [HttpPost]
        public ActionResult Login1212427(FormCollection f)
        {
            Account acc = new Account(f["username"], f["password"]);
            System.Web.HttpContext.Current.Session["account"] =  AccountDAO.Login(acc);
            return RedirectToAction("Index1212427", "users"); //Redirect(Request.QueryString["returnUrl"]);
        }
        
        public ActionResult Logout1212427()
        {
            System.Web.HttpContext.Current.Session["account"] = null;
            return RedirectToAction("Index1212427","Home");
        }
    }
}
