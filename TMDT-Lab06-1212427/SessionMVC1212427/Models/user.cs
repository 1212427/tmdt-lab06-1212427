﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SessionMVC1212427.Models
{
    public class user
    {
        private string mssv;
        public string Mssv
        {
            get { return mssv; }
            set { mssv = value; }
        }

        private string hoten;
        public string Hoten
        {
            get { return hoten; }
            set { hoten = value; }
        }
        public user(string _mssv, string _hoten)
        {
            this.Mssv = _mssv;
            this.Hoten = _hoten;
        }
    }
}