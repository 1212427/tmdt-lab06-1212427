﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SessionMVC1212427.Models
{
    public class users
    {
        private List<user> us;
        public List<user> Users
        {
            get { return us; }
            set { us = value; }
        }

        public users()
        {
            this.Users = new List<user>();
            this.Users.Add(new user("1212387", "Nguyễn Trần Phước Thọ"));
            this.Users.Add(new user("1212427", "Hồ Thế Tông"));
            this.Users.Add(new user("1212436", "Phan Hiền Triết"));
            this.Users.Add(new user("1212439", "Đoàn Xuân Trí"));
            this.Users.Add(new user("1212446", "Trương Mai Minh Trí"));
            this.Users.Add(new user("1212511", "Nguyễn Hữu Vinh"));
        }
    }
}