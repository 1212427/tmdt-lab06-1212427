﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SessionMVC1212427.Models
{
    public class Account
    {
        private string username;
        public string Username
        {
            get { return username; }
            set { username = value; }
        }

        private string password;
        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        public Account(string _username, string _password)
        {
            this.Username = _username;
            this.Password = _password;
        }
    }
}